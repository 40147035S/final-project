#ifndef OBJ_LOADER_H_INCLUDED
#define OBJ_LOADER_H_INCLUDED

#include <windows.h>
#define GLEW_STATIC

#include <vector>
#include "position.h"

class OBJ
{
public:
//建構子
    OBJ () = default;
    OBJ (const OBJ& tmp) :P1(tmp.P1),myObj(tmp.myObj) {}

//自檔案讀檔
    void Read(char filename[]) {
        myObj = glmReadOBJ(filename);
    }
//縮小至單位矩陣
    void Unitize() {
        glmUnitize(myObj);
    }
//畫出物體
    void drawOBJ();
private:
    position P1;
    GLMmodel *myObj ;
};

class OBJ_Group
{
public:
    OBJ_Group() { Group.clear(); }

    void Read(char filename[][20],size_t num);
    void drawOBJs();
    void Unitize();
private:
    std::vector<OBJ> Group;
};

#endif // OBJ_LOADER_H_INCLUDED
