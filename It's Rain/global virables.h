#ifndef GLOBAL_VIRABLES_H_INCLUDED
#define GLOBAL_VIRABLES_H_INCLUDED


const float Pi=3.1415926;
const double UNIT = 0.005, ANGLE = 3.0, DISTANCE = 5.0;

GLMmodel *myObj = NULL;

GLfloat light_theta=0.0;
GLdouble centerX = 5.0, centerY = -0.3, centerZ = 0;

GLhandleARB v,f,f2,p;
GLfloat light0_pos[]= {0.0, 1.0, 0.0, 1.0};
GLfloat light0_ambient [] = {1.0, 1.0, 0.8, 1.0};
GLfloat light0_diffuse [] = {1.0, 1.0, 1.0, 1.0};
GLfloat light0_specular[] = {1.0, 1.0, 1.0, 1.0};
GLfloat light0Direction[] = {0.0,1.0,0.0};

//int shading_mode=0;
int left_button_state=0,d=0;
int mouseState,mouseButton;
int mouseX,mouseY;
float eyeX=0,eyeY=0,eyeZ=0,degree=0,upper=0,raise=0;

#endif // GLOBAL_VIRABLES_H_INCLUDED
