#include "enviro.h"

lighting::lighting(float x1,float y1,float z1)
:light0_pos(x1,y1,z1)
{
    light_theta = 0.0;
    spotlight = false;
}


void lighting::open_the_light()
{
    GLfloat light0_[]= {0.0, 1.0, 0.0, 1.0};

    light0_[0] = light0_pos.x_();
    light0_[1] = light0_pos.y_();
    light0_[2] = light0_pos.z_();

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glLightfv(GL_LIGHT0, GL_POSITION, light0_ );
}

void lighting::theta_plus()
{
    light_theta+=Pi/12;
    if(light_theta>=Pi*2)
    {
        light_theta-=Pi*2;
    }
    light0_pos.new_x( sin(light_theta) );
    light0_pos.new_y( cos(light_theta) );
}
void lighting::theta_minus()
{
    light_theta-=Pi/12;
    if(light_theta<=0)
    {
        light_theta+=Pi*2;
    }
    light0_pos.new_x( sin(light_theta) );
    light0_pos.new_y( cos(light_theta) );
}
