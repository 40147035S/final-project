#include "watcher.h"


Watcher::Watcher (const float eyeX,const float eyeY,const float eyeZ,
             const float deg,const float up,const float rise)
         :EYE(eyeX,eyeY,eyeZ),degree(deg),upper(up),raise(rise){}
//LookAt
void Watcher::LookAT()
{
    gluLookAt( EYE.x_(), EYE.y_() + raise, EYE.z_(),
               EYE.x_() + cos( degree * Pi / 180 ),
               EYE.y_() + sin( upper * Pi /180 ) + raise,
               EYE.z_() + sin( degree * Pi / 180 ),
                     0.0,      1.0,      0.0 );
}

void Watcher::Move_Forward(const float DEG)
{
    EYE.x_Amov( cos(degree*Pi/180) * DEG * Magnify );
    EYE.y_Amov( sin(upper*Pi/180) * DEG * Magnify );
    EYE.z_Amov( sin(degree*Pi/180) * DEG * Magnify );
}

void Watcher::Move_Backward(const float DEG)
{
    EYE.x_Mmov( cos(degree*Pi/180) * DEG * Magnify) ;
    EYE.y_Mmov( sin(upper*Pi/180) * DEG * Magnify );
    EYE.z_Mmov( sin(degree*Pi/180) * DEG * Magnify );
}

void Watcher::Move_Left(const float DEG)
{
    EYE.x_Amov( sin(degree*Pi/180) * DEG * Magnify );
    EYE.z_Mmov( cos(degree*Pi/180) * DEG * Magnify );
}
void Watcher::Move_Right(const float DEG)
{
    EYE.x_Mmov( sin(degree*Pi/180)* DEG * Magnify );
    EYE.z_Amov( cos(degree*Pi/180)* DEG * Magnify );
}

void Watcher::Add_degree(size_t deg)
{
    degree += deg;
    if( degree > 360 ) degree -= 360;
}
void Watcher::Minus_degree(size_t deg)
{
    degree -= deg;
    if( degree < 0 ) degree += 360;
}

void Watcher::Add_upper(size_t up)
{
    upper += up;
    if( upper > 90 ) upper = 90;
}

void Watcher::Minus_upper(size_t up)
{
    upper -= up;
    if( upper < -90 ) upper = -90;
}
