#include "OBJ_loader.h"

using namespace std;

void OBJ::drawOBJ()
{
 if (!myObj) return;

    for (GLMgroup *groups = myObj->groups; groups != NULL; groups = groups->next) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, myObj->materials[groups->material].textureID);
        for(unsigned i=0; i<groups->numtriangles; i+=1) {
            glBegin(GL_TRIANGLES);
                for (int j=0; j<3; j+=1)
                {
                    glNormal3fv(&myObj->normals[myObj->triangles[groups->triangles[i]].nindices[j]*3]);
                    glTexCoord2fv(&myObj->texcoords[myObj->triangles[groups->triangles[i]].tindices[j]*2]);
                    glVertex3fv(&myObj->vertices[myObj->triangles[groups->triangles[i]].vindices[j]*3]);
                }
            glEnd();
        }
    }
}

void OBJ_Group::Read(char filename[][20],size_t num)
{
    Group.clear();
    for( size_t i=0; i<num; i++ ) {
        OBJ tmp;
        tmp.Read( filename[i] );
        Group.push_back(tmp);
    }
}

void OBJ_Group::drawOBJs()
{
    for( size_t i=0; i<Group.size(); i++ ) {
        Group[i].drawOBJ();
    }
}

void OBJ_Group::Unitize()
{
    for( size_t i=0; i<Group.size(); i++ ) {
        Group[i].Unitize();
    }
}
