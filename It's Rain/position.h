#ifndef POSITION_H_INCLUDED
#define POSITION_H_INCLUDED

//#include <stdio.h>
#include <stdlib.h>
#include <GL/glew.h>
#include "glm.h"
#include <math.h>

//關於位移，物體位置的相關動作
static const float Pi = 3.1415926;

class position
{
public:
//建構子
    position () {
        x=0,y=0,z=0;
    };
    position(float x1,float y1,float z1)
        :x(x1),y(y1),z(z1) {}
    position( const position &tmp ) :position(tmp.x,tmp.y,tmp.z) {}
//------for the watcher--------------

    float x_ () const { return x; }
    float y_ () const { return y; }
    float z_ () const { return z; }

//-----------------------------------

//新的位置
    void new_pos (float x,float y,float);
    //x
    void new_x (float pos) { x = pos; }
    //y
    void new_y (float pos) { y = pos; }
    //z
    void new_z (float pos) { z = pos; }
//__軸上的位移
    //x_
    void x_Mmov (const float mov) { x -= mov; }
    void x_Amov (const float mov) { x += mov; }
    //y_
    void y_Mmov (const float mov) { y -= mov; }
    void y_Amov (const float mov) { y += mov; }
    //z_
    void z_Mmov (const float mov) { z -= mov; }
    void z_Amov (const float mov) { z += mov; }
//對__軸旋轉

private:
    float x,y,z;
};

#endif // POSITION_H_INCLUDED
