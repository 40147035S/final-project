#ifndef WATCHER_H_INCLUDED
#define WATCHER_H_INCLUDED

#include "position.h"

class Watcher
{
public:
//建構子
    Watcher () { }
    Watcher (const float eyeX,const float eyeY,const float eyeZ,
             const float deg,const float up,const float rise);
//LookAt
    void LookAT();
//關於眼睛
    //位置調整
    //Forward
        void Move_Forward(const float DEG);
    //Backward
        void Move_Backward(const float DEG);
    //To your Left
        void Move_Left(const float DEG);
    //To your Right
        void Move_Right(const float DEG);
    //角度調整
    void Add_degree(size_t deg);
    void Minus_degree(size_t deg);
    //俯仰角調整
    void Add_upper(size_t up);
    void Minus_upper(size_t up);
    void Back_to_Horizon() { upper = 0; }
    //上下位移
    void Add_raise(const float rise) { raise += rise * Magnify; }
    void Minus_raise(const float rise) { raise -= rise * Magnify; }

    void Set_the_Magnify(const float M1) { Magnify = M1; }
private:
    position EYE;
    float degree,upper,raise;
    float Magnify = 1;
};

class Mouse
{
public:
    Mouse() { left_button_state = 0; }

    bool is_LeftBut_Clic () const { return (left_button_state==1); }

    //value return
    int M_State() const { return mouseState; }
    int M_Button() const { return mouseButton; }
    int _MouseX() const { return mouseX; }
    int _MouseY() const { return mouseY; }

    //value assign
    void left_button (int lb) { left_button_state = lb; }
    void MState_alter(int state) { mouseState = state; }
    void MButton_alter(int btn) { mouseButton = btn; }
    void New_Position(int x,int y) { mouseX = x,mouseY = y; }
private:
    int left_button_state;      //about the left button
    int mouseState,mouseButton; //mouse's state
    int mouseX,mouseY;          //mouse on the monitor
};

#endif
