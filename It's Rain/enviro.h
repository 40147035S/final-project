#ifndef ENVIRONMENT_H_INCLUDED
#define ENVIRONMENT_H_INCLUDED

#include "position.h"

class lighting
{
public:
    lighting(float x1,float y1,float z1);

    void open_the_light();
    void spot_switch() { spotlight = !spotlight; }

    void theta_plus();
    void theta_minus();

private:
    position light0_pos;
    GLfloat light_theta;
    bool spotlight;
};


#endif // ENVIRONMENT_H_INCLUDED
